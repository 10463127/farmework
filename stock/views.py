from datetime import time
from stock.models import stock_price
from django.shortcuts import render
from django.http.response import JsonResponse, HttpResponse
import csv
import requests as s
# Create your views here.

def getCSVStock(request, date, stock_no):
    url = 'https://www.twse.com.tw/exchangeReport/STOCK_DAY_AVG?response=csv&date={}&stockNo={}'.format(date, stock_no)
    res = s.get(url)
    decode_content = res.content.decode('big5')
    rows = csv.reader(decode_content.splitlines(), delimiter=',')
    data_list = list(rows)
    # stock_sn = "{}-{}{}{}".format(stock_no, int(timestr[0]) + 1911, timestr[1], timestr[2])
    # stock_no = stock_no
    # datetime = "{}-{}-{}".format(int(timestr[0]) + 1911, timestr[1], timestr[2])
    # close_price = data[1]
    # predict_price = 0
    db_list = list()
    for data in data_list:
        try:
            timestr = data[0].split('/')
        except:
            timestr = None
        if timestr:
            if len(timestr) == 3:
                ISOdateStr = "{}-{}-{}".format(int(timestr[0])+1911, timestr[1], timestr[2])
                SerNo = "{}-{}{}{}".format(stock_no, int(timestr[0])+1911, timestr[1], timestr[2])
                db_list.append([SerNo, stock_no, ISOdateStr, data[1], '0'])
                # Store to database
                # 1. Check record is existed or not.
                # 2. Save to database.
                try:
                    p = stock_price.objects.get(stock_sn = SerNo)
                except:
                    p = None

                if p == None:
                    p = stock_price(stock_sn = SerNo, stock_no = stock_no, datetime = ISOdateStr, close_price=data[1], predict_price='0')
                    p.save()

    return HttpResponse(db_list)


