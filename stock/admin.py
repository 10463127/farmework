from django.contrib import admin
from stock.models import stock_price

class AdminStock(admin.ModelAdmin):
    list_display = ('stock_sn', 'stock_no', 'datetime', 'close_price', 'predict_price')
# Register your models here.
admin.site.register(stock_price, AdminStock)