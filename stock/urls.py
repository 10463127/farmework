from django.urls import path
from stock.views import * 

urlpatterns = [
    path('getdata/<str:date>/<str:stock_no>', getCSVStock)
]