from django.db import models

# Create your models here.
class stock_price(models.Model):
    stock_sn = models.CharField(max_length=20, unique=True, verbose_name='股票代號-時間戳記')
    stock_no = models.CharField(max_length=4, verbose_name='股票代號')
    datetime = models.DateField(verbose_name='日期')
    close_price = models.FloatField(verbose_name='收盤價')
    predict_price = models.FloatField(verbose_name = '預測價格')

    def __str__(self):
        return self.stock_sn
