## 3 種版本上傳方式

* 功能都在 gui 的 app 底下
* 需要新增 file 資料夾 (如果部屬到伺服器需要給這個資料夾 chmod -R 777 權限)
* AJAX 請求上傳檔案需要關閉 CSRF (到 settings.py 找 django.middleware.csrf.CsrfViewMiddleware --> 註解)

1. http://127.0.0.1/gui/f_upload.html (簡單版本)
2. http://127.0.0.1/gui/ja_upload.html (JS 版本)
3. http://127.0.0.1/gui/jq_upload.html (JQuery 版本)