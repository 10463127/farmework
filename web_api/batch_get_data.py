import sys
import time
import requests as rs

if __name__ == '__main__':
    try:
        api_url = sys.argv[1]
        stock_no = sys.argv[2]
    except IndexError:
        api_url = 'http://127.0.0.1/'
        stock_no = '2330'
    
    param_yr = ['1999','2000','2001','2002','2003','2004','2005', \
                '2006', '2007', '2008','2009','2010', '2011', '2012', \
                '2013',  '2014', '2015', '2016', '2017', '2018', '2019', \
                '2020', '2021'
                ]
    param_mon=['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
    
    for yr in param_yr:
        for month in param_mon:
            url = '{}/stock/getdata/{}{}01/{}'.format(api_url, yr, month, stock_no)
            time.sleep(10)
            rs.get(url)


    #rs.get('http://127.0.0.1:8000/stock/getdata/{}{}01/{}'.format('2021','08', '2330'))
    #print("Test Function. arg1 = {}, arg2 = {}".format(arg1, arg2))