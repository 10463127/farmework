from stock.views import getCSVStock
from django.urls import path
from web_api.views import * 

urlpatterns = [
    path('', home),
    path('log/', getRfid),
    path('getStockJson/', getStockJson),
    path('runpy/', runpy), # 127.0.0.1/api/runpy
    path('batch/', batch_runpy)
]