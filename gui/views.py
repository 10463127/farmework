from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'data_view.html', locals())


def F_Upload(request):                 # Submit upload file in form
    if request.method == "GET":
        return render(request, 'form_upload.html', )
    else:
        F = request.FILES
        print("form The uploaded file is:",F)                   #You can see a dictionary that protects file names and file objects
        f_obj = F.get('uploadfile')                     #Uploaded file object

        N = f_obj.name                                  #Uploaded file name
        S = f_obj.size                                  #Uploaded file size

        print("The file name is:",N)
        print("The file size is:",S)

        W_File(f_obj)                                   #How to upload files
        return HttpResponse('File upload succeeded!')

def W_File(file_obj):                                   #Upload file is written to the server, and the parameter is the uploaded file object
    f = open('gui/file/' + file_obj.name + "", 'wb')    # The server creates and uploads a file with the same name
    for line in file_obj.chunks():                      # Take and upload data in blocks
        f.write(line)                                   # Write the obtained data block to the server circularly
    f.close()


def JA_Upload(request):
    if request.method == "GET":
        return render(request,'js_ajax_upload.html',)
    else:
        file_obj = request.FILES.get('file')                # Get the file data from
        print("The name of the uploaded file is:", file_obj.name)
        print("The size of the uploaded file is:", file_obj.size)

        f = open('gui/file/' + file_obj.name + "", 'wb')    # The server creates and uploads a file with the same name
        for line in file_obj.chunks():                      # Take and upload data in blocks
            f.write(line)                                   # Write the obtained data block to the server circularly
        f.close()
        return HttpResponse('File upload succeeded!')

def JQ_Upload(request):
    if request.method == "GET":
        return render(request,'jquery_ajax_upload.html',)
    else:
        file_obj = request.FILES.get('file')                # Get the file data from
        print("The name of the uploaded file is:", file_obj.name)
        print("The size of the uploaded file is:", file_obj.size)

        f = open('gui/file/' + file_obj.name + "", 'wb')    # The server creates and uploads a file with the same name
        for line in file_obj.chunks():                      # Take and upload data in blocks
            f.write(line)                                   # Write the obtained data block to the server circularly
        f.close()
        return HttpResponse(file_obj.name)