from django.urls import path,re_path
from gui.views import * 

urlpatterns = [
    path('', home),
    re_path('^f_upload.html$',F_Upload),
    re_path('^ja_upload.html$',JA_Upload),
    re_path('^jq_upload.html$',JQ_Upload),
]